var url= "";
var inputWhere;

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, '\\$&');
   var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
   results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

var linkToCategories = document.getElementById("linkToCategories");
var linkToGastroList = document.getElementById("linkToGastroList");

if (getParameterByName('place') != undefined) {
   inputWhere = getParameterByName('place');
   document.getElementById("where").value = getParameterByName('place');

   linkToCategories.href = "categories.html?place=" + getParameterByName('place');
   linkToGastroList.href = "selectedCategory_list.html?id=gastro&?place=" + getParameterByName('place');
}

// Function for collapsing Navigation
function collapsNavi() {
   let x = document.getElementById("topnavi");
   let y = document.getElementById("link-button-image");
   if (x.className === "navi") {
      x.className += " responsive";
      y.style.display = "none";
   } else {
      x.className = "navi";
      y.style.display = "flex";
   }
}

// Search-function for what
function getWhat() {
   var input = document.getElementById("what").value;
   document.getElementById("what").addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
         if (getParameterByName('place') != undefined) {
            window.location = "selectedCategory_list.html?place=" + inputWhere + "&?input=" + input;
         } else {
            window.location = "selectedCategory_list.html?input=" + input;
         }
      }
   });

   return false;
}

// Search-function for where
function getWhere() {
   window.location = "filterSearchWhere.html";
}