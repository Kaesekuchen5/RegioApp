var url= "";
var display = document.getElementById("list_shop");
var category_id;
var inputText;
var latitude;
var longitude;
var place;

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, '\\$&');
   var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
   results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

   if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
         function (position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
       
            var id = category_id = getParameterByName('id');
            var place1 = getParameterByName("place");
            if (place1 != null) {
               place = place1.replaceAll(" ", "%20");
            }
            if ((id != undefined && place == undefined) || (id != undefined && place1 == "Aktueller Standort")) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?lat=" + latitude + "&lng=" + longitude + "&id=" + id;
               console.log(url);
               // Set new Title
               if (id == "gastro") {
                  document.title = "Regional essen";
               } else {
                  var category = ["Milch & Milchprodukte", "Mehl & Getreide", "Nudeln & Teigwaren", "Obst & Gem\u00fcse", "Backwaren", "Fisch, Fleisch & Wurst", "Aufstriche & So\u00dfen", "Eier", "Getr\u00e4nke", "Essig & \u00d6l", "Eingemachtes", "Blumen & Pflanzen", "Wochenm\u00e4rkte", "Alle Verkaufsstellen", "Kleine L\u00e4den", "Marktschw\u00e4rmer", "Dorfl\u00e4den"];
                  document.title = category[parseInt(id) - 1];
               }
            } else if ((id != undefined) && (place != undefined)) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?location=" + place + "&id=" + id;
               console.log(url);
            } else {
               var input = inputText = getParameterByName('input');
               if (place != undefined && place1 != "Aktueller Standort") {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?location=" + place + "&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               } else {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?lat=" + latitude + "&lng=" + longitude + "&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               }
            }
            
            startShop();
         },function(error) {
            var id = category_id = getParameterByName('id');
            var place1 = getParameterByName("place");
            if (place1 != null) {
               place = place1.replace(" ", "%20");
            }
            if ((id != undefined && place == undefined) || (id != undefined && place1 == "Aktueller Standort")) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&location=" + place + "&id=" + id;
               console.log(url);
               // Set new Title
               if (id == "gastro") {
                  document.title = "Regional essen";
               } else {
                  var category = ["Milch & Milchprodukte", "Mehl & Getreide", "Nudeln & Teigwaren", "Obst & Gem\u00fcse", "Backwaren", "Fisch, Fleisch & Wurst", "Aufstriche & So\u00dfen", "Eier", "Getr\u00e4nke", "Essig & \u00d6l", "Eingemachtes", "Blumen & Pflanzen", "Wochenm\u00e4rkte", "Alle Verkaufsstellen", "Kleine L\u00e4den", "Marktschw\u00e4rmer", "Dorfl\u00e4den"];
                  document.title = category[parseInt(id) - 1];
               }
            } else if ((id != undefined) && (place != undefined)) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&location=" + place + "&id=" + id;
               console.log(url);
            } else {
               var input = inputText = getParameterByName('input');
               if (place != undefined && place1 != "Aktueller Standort") {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&location=" + place + "&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               } else {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               }
            }
            startShop();
          });
   } else {
     geoinfo.innerHTML = 'Dieser Browser unterstützt die Abfrage der Geolocation nicht.';
   }

   function startShop(){
      var xhr = new XMLHttpRequest();
      xhr.open("GET", url);
      
      xhr.onreadystatechange = function start() {
         if (xhr.readyState === 4) {
            console.log(xhr.status);
            var ourData =JSON.parse(xhr.responseText);
            renderHTML(ourData);
         }};
      
         function renderHTML(data){

            if (data.length <= 2) {
               // Remove Scroll-Button if only 2 shops are visible
               var scrollButton = document.getElementById("btnScrollToTop");
               scrollButton.remove();
            }
            // If result is empty
            if (data.length == 0 && category_id == undefined) {
               var error_div = document.createElement("div");
               error_div.className = "error_div";
               error_div.innerText = "Keine Ergebnisse für " + "'" + inputText + "'";
               display.appendChild(error_div);
            }
            
            /*Nur die ersten 150 Ergebnisse werden angezeigt oder 300 Ergebnisse aus ganz D.*/
            for (var x = 0; x < data.length; x++){
               // Create button with shop & link
               var fig = document.createElement("figure");
               fig.className = "fig_for_shop_list";
               // Last button without border
               if ((x > 3) && ((x == (data.length - 1)))) {
                  var list_button_last = document.createElement("button");
                  list_button_last.id = "last_shop";
               } else {
                  var list_button = document.createElement("button");
                  list_button.className = "shops";
               }

               var link = document.createElement('a');
               link.setAttribute("href","selectedShop.html?id=" + data[x].id);

               // Div for Name, adress & products
               var list_container = document.createElement("div");
               list_container.className = "list_container";
               // Last button without border
               if ((x > 3) && ((x == (data.length - 1)))) {
                  list_button_last.appendChild(list_container);
               } else {
                  list_button.appendChild(list_container);
               }
               
               // Name of the shop
               var name = document.createElement("h2");
               name.innerHTML = data[x].name;
               list_container.appendChild(name);

               // Adress of shop
               var adress = document.createElement("p");
               adress.innerHTML = data[x].adresse + "\n" + data[x].plz + " " + data[x].ort;
               list_container.appendChild(adress);

               // Image for open or 'ist_verkaeufer' or 'hat_lieferservice'
               if (data[x].isOpen) {
                  var image = document.createElement("img");
                  image.className = "image_of_shops_is_open";
                  image.src = "https://www.regioapp.org/app/regioapp/images/store_open.svg";
                  list_container.appendChild(image);
               } else {
                  var image = document.createElement("img");
                  image.className = "image_of_shops_is_open";
                  image.src = "https://www.regioapp.org/app/regioapp/images/store_closed.svg";
                  list_container.appendChild(image);
               }

               if(data[x].ist_verkaeufer == 1) {
                  var image = document.createElement("img");
                  image.src = "https://www.regioapp.org/app/regioapp/images/is_seller.svg";
                  list_container.appendChild(image);
               }

               if (data[x].hat_lieferservice == 1) {
                  var image = document.createElement("img");
                  image.className = "image_of_shops_has_lieferservice";
                  image.src = "https://www.regioapp.org/app/regioapp/images/icon-hat-lieferservice.png";
                  list_container.appendChild(image);
               }


               // Products of shop
               var products = document.createElement("p");
               products.innerHTML = data[x].products;
               list_container.appendChild(products);
               
               // Image of the shop
               var image_div = document.createElement("div");
               image_div.className = "image_div";
               var image = document.createElement("img");
               image.className = "img_of_shops";
               // Catch null images
               if (data[x].image !== null) {
                  image.src = "https://regionalbewegung.de/regioapp/data/images/profiles/" + data[x].id + "/" + data[x].image;

                  image_div.appendChild(image);
                  // Last button without border
                  if ((x > 3) && ((x == (data.length - 1)))) {
                     list_button_last.appendChild(image_div);
                  } else {
                     list_button.appendChild(image_div);
                  }
               }
               // Last button without border
               if ((x > 3) && ((x == (data.length - 1)))) {
                  list_button_last.appendChild(image_div);
               } else {
                  list_button.appendChild(image_div);
               }

               // Certification of shop
               var certification_div = document.createElement("div");
               certification_div.className = "certification_div";
               var certification = document.createElement("img");
               certification.className = "img_certification";
               if (data[x].certification !== (null || "")) {
                  certification.src = "https://regionalbewegung.de/regioapp/data/images/certifications/" + data[x].certification;

                  certification_div.appendChild(certification);
                  // Last button without border
                  if ((x > 3) && ((x == (data.length - 1)))) {
                     list_button_last.appendChild(certification_div);
                  } else {
                     list_button.appendChild(certification_div);
                  }
               }
               // Last button without border
               if ((x > 3) && ((x == (data.length - 1)))) {
                  list_button_last.appendChild(certification_div);
               } else {
                  list_button.appendChild(certification_div);
               }

               // distance & ui-icon-arrow-r
               var div_for_km_icon = document.createElement("div");
               div_for_km_icon.className = "div_for_km_icon";
               
               var distance = document.createElement("p");
               var dis = data[x].dist;
               
               var icon_image = document.createElement("img");
               icon_image.className = "ui-icon-arrow-r";
               icon_image.src = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjQsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkViZW5lXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB3aWR0aD0iMTIuNDIycHgiIGhlaWdodD0iMjEuNjY4cHgiIHZpZXdCb3g9IjAgMCAxMi40MjIgMjEuNjY4IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMi40MjIgMjEuNjY4IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxwb2x5bGluZSBmaWxsPSJub25lIiBzdHJva2U9IiNENzAwMjkiIHN0cm9rZS13aWR0aD0iMy4xNzciIHN0cm9rZS1saW5lY2FwPSJyb3VuZCIgc3Ryb2tlLWxpbmVqb2luPSJyb3VuZCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludHM9Ig0KCTEuNTg4LDEuNTg4IDEwLjgzNCwxMC44MzMgMS41ODgsMjAuMDc5ICIvPg0KPC9zdmc+DQo=";
               
               if (dis !== "false") {
                  distance.innerText = dis.split(".")[0] + " km";
               } else {
                  const sheet = new CSSStyleSheet();
                  sheet.replaceSync('.ui-icon-arrow-r {width: 40%; height: 40%; object-fit: contain; margin-top: 21%;}');

                  // Apply the stylesheet to a document:
                  document.adoptedStyleSheets = [sheet];
               }

               div_for_km_icon.appendChild(distance);
               div_for_km_icon.appendChild(icon_image);

               // Last button without border
               if ((x > 3) && ((x == (data.length - 1)))) {
                  list_button_last.appendChild(div_for_km_icon);
               } else {
                  list_button.appendChild(div_for_km_icon);
               }

               // Link
               // Last button without border
               if ((x > 3) && ((x == (data.length - 1)))) {
                  fig.appendChild(list_button_last);
                  fig.appendChild(link);
                  link.appendChild(list_button_last);
               } else {
                  fig.appendChild(list_button);
                  fig.appendChild(link);
                  link.appendChild(list_button);
               }

               display.appendChild(fig);
            }
      }
      xhr.send();

      // Remove loader
      var loader = document.getElementById("spinner");
      loader.remove();
   }

// Start Map
function startMap() {
   if ((category_id != undefined) && (place == undefined)) {
      window.location = "selectedCategory_map.html?id=" + category_id;
   } else if ((category_id != undefined) && (place != undefined)) {
      window.location = "selectedCategory_map.html?id=" + category_id + "&?place=" + place;
   } else {
      if (place != undefined) {
         window.location = "selectedCategory_map.html?input=" + inputText + "&?place=" + place;
      } else {
         window.location = "selectedCategory_map.html?input=" + inputText;
      }
   }
}