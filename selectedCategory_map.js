var url= "";
var display = document.getElementById("map");
var latitude;
var longitude;
var category_id;
var inputText;
var place;

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

   if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
         function (position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            
            var id = category_id = getParameterByName('id');
            var place1 = getParameterByName("place");
            if (place1 != null) {
               place = place1.replaceAll(" ", "%20");
            }
            if ((id != undefined && place == undefined) || (id != undefined && place1 == "Aktueller Standort")) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?lat=" + latitude + "&lng=" + longitude + "&id=" + id;
               console.log(url);

               // Set new Title
               if (id == "gastro") {
                  document.title = "Regional essen";
               } else {
                  var category = ["Milch & Milchprodukte", "Mehl & Getreide", "Nudeln & Teigwaren", "Obst & Gem\u00fcse", "Backwaren", "Fisch, Fleisch & Wurst", "Aufstriche & So\u00dfen", "Eier", "Getr\u00e4nke", "Essig & \u00d6l", "Eingemachtes", "Blumen & Pflanzen", "Wochenm\u00e4rkte", "Alle Verkaufsstellen", "Kleine L\u00e4den", "Marktschw\u00e4rmer", "Dorfl\u00e4den"];
                  document.title = category[parseInt(id) - 1];
               }
            } else if ((id != undefined) && (place != undefined)) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?location=" + place + "&id=" + id;
               console.log(url);
            } else {
               var input = inputText = getParameterByName('input');
               if (place != undefined && place1 != "Aktueller Standort") {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?location=" + place + "&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               } else {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?lat=" + latitude + "&lng=" + longitude + "&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               }
            }
            
            startShop();
         },function(error) {
            var id = category_id = getParameterByName('id');
            var place1 = getParameterByName("place");
            if (place1 != null) {
               place = place1.replace(" ", "%20");
            }
            if ((id != undefined && place == undefined) || (id != undefined && place1 == "Aktueller Standort")) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&location=" + place + "&id=" + id;
               console.log(url);
               // Set new Title
               if (id == "gastro") {
                  document.title = "Regional essen";
               } else {
                  var category = ["Milch & Milchprodukte", "Mehl & Getreide", "Nudeln & Teigwaren", "Obst & Gem\u00fcse", "Backwaren", "Fisch, Fleisch & Wurst", "Aufstriche & So\u00dfen", "Eier", "Getr\u00e4nke", "Essig & \u00d6l", "Eingemachtes", "Blumen & Pflanzen", "Wochenm\u00e4rkte", "Alle Verkaufsstellen", "Kleine L\u00e4den", "Marktschw\u00e4rmer", "Dorfl\u00e4den"];
                  document.title = category[parseInt(id) - 1];
               }
            } else if ((id != undefined) && (place != undefined)) {
               url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&location=" + place + "&id=" + id;
               console.log(url);
            } else {
               var input = inputText = getParameterByName('input');
               if (place != undefined && place1 != "Aktueller Standort") {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&location=" + place + "&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               } else {
                  url = "https://www.regionalbewegung.de/regioapp/data/category-v3.php?locationManuallySet=true&filter=" + input;
                  console.log(url);
                  var text = document.getElementById("what");
                  text.value = input;
               }
            }
            startShop();
          });
   } else {
     geoinfo.innerHTML = 'Dieser Browser unterstützt die Abfrage der Geolocation nicht.';
   }

   function startShop(){
      // Set button:
      const sheet = new CSSStyleSheet();
      sheet.replaceSync('#map_button {background-color: #cccdcf; color: black;} #list_button {background-color: #f4f4f4; color: #b2bf2c;} #map_button:hover {background-color: #ddd; border-color: #ddd;} #list_button:hover {background-color: #fcfcfc;}');

      // Apply the stylesheet to a document:
      document.adoptedStyleSheets = [sheet];

      var xhr = new XMLHttpRequest();
      xhr.open("GET", url);
      
      xhr.onreadystatechange = function start() {
        if (xhr.readyState === 4) {
          console.log(xhr.status);
          var ourData =JSON.parse(xhr.responseText);
          renderHTML(ourData);
          
        }};
      
        function renderHTML(data){
          
          //lat, Lng
          var locations= new Array();
          for (var x = 0; x < data.length; x++){
            locations.push([data[x].name, data[x].id, data[x].products, data[x].lat, data[x].lng]);
          }
          // Map
          const myLatLng = { lat: latitude, lng:longitude };
          const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 9,
            center: new google.maps.LatLng(latitude, longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          var infowindow = new google.maps.InfoWindow();
          var marker, i;

          if (latitude != undefined && longitude != undefined) {
            const myLatLng = { lat: latitude, lng:longitude };

            var own_marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                icon: "https://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&scale=1.1",
                title: "Dein Standort",
                map: map
              });

            own_marker.addListener("click", () => {
              infowindow.setContent("Dein Standort");
              infowindow.open(map, own_marker);
            });
          }

          var bounds = new google.maps.LatLngBounds();

          for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][3], locations[i][4]),
              title: locations[i][0],
              map: map,
            });

            var href = "selectedShop.html?id=" + locations[i][1];
            const contentString = 
            '<div id="marker_content">' +
              '<h2>' + locations[i][0] + '</h2>' +
              '<p>' + locations[i][2] + '</p>' +
              '<a id="marker_link" href=' + href + ">" + "Mehr anzeigen" + '</a>'
            '</div>';
  
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(contentString);
              infowindow.open(map, marker);
              }
            })(marker, i));

            // Make all marker visible in one map
            if (latitude != undefined && longitude != undefined) {
              bounds.extend(own_marker.position);
            }
            bounds.extend(marker.position);
            map.setCenter(bounds.getCenter);
            map.fitBounds(bounds);
          }
        }
    xhr.send();
  }

// Back to list view
function backtoList() {
  if ((category_id != undefined) && (place == undefined)) {
    window.location = "selectedCategory_list.html?id=" + category_id;
 } else if ((category_id != undefined) && (place != undefined)) {
    window.location = "selectedCategory_list.html?id=" + category_id + "&?place=" + place;
 } else {
    if (place != undefined) {
       window.location = "selectedCategory_list.html?input=" + inputText + "&?place=" + place;
    } else {
       window.location = "selectedCategory_list.html?input=" + inputText;
    }
 }
}