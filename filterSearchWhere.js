var place_div = document.getElementById("server_input1");

var url= "";

function getPlaces() {
    document.getElementById("server_input1").innerHTML = "";
    var input = document.getElementById("input_where").value;
    url = "https://regionalbewegung.de/regioapp/data/locations-v4.php?f=" + input;

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    
    xhr.onreadystatechange = function () {
       if (xhr.readyState === 4) {
          console.log(xhr.status);
         //console.log(xhr.responseText);
          var ourData =JSON.parse(xhr.responseText);
            renderHTML(ourData);
       }};
    
    xhr.send();
}

function startServer() {
    var xhr = new XMLHttpRequest();
        console.log(url);
        xhr.open("GET", url, true);
        
        xhr.onreadystatechange = handleRequestStateChange;
}

var handleRequestStateChange = function start() {
    if (xhr.readyState === 4) {
        var ourData =JSON.parse(xhr.responseText);
        renderHTML(ourData);
    }};

function renderHTML(data){
  
    for (var x = 0; x < data.length; x++){

        var list_container = document.createElement("div");

        if ((data.length >= 6) && (x === (data.length - 1))) {
            list_container.className = "last_list_containerWhere";
        } else {
            list_container.className = "list_containerWhere";
        }
        var list_button = document.createElement("button");
        var figure = document.createElement("figure");
        var link = document.createElement('a');

        var href = "" + document.referrer;
        var place = data[x].replaceAll(" ", "%20");
        if (href.includes("?place=")) {
            href = href.substring(0, href.indexOf("?place="));
        }
        if (href.includes("?")) {
            link.setAttribute("href", href + "&?place=" + place);
        } else {
            link.setAttribute("href", href + "?place=" + place);
        }

        list_button.className = "list_buttonWhere";
        var outputServer = document.createElement("p");
        outputServer.className="textWhere"
        outputServer.innerHTML = data[x];
        list_button.appendChild(outputServer);

        figure.appendChild(list_button);
        figure.appendChild(link);
        link.appendChild(list_button);

        list_container.appendChild(figure);

        place_div.appendChild(list_container);
    }
}

// "Aktueller Standort"
function goBack() {
    var href = "" + document.referrer;
    if (href.includes("?place=")) {
        href = href.substring(0, href.indexOf("?place="));
    }

    if (href.includes("?")) {
        window.location = href + "&?place=Aktueller%20Standort";
    } else {
        window.location = href + "?place=Aktueller%20Standort";
    }
}
