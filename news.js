var url = "https://regionalbewegung.de/regioapp/data/news-v2.php";
var display = document.getElementById("news");

var xhr = new XMLHttpRequest();
xhr.open("GET", url);

xhr.onreadystatechange = function () {
   if (xhr.readyState === 4) {
      console.log(xhr.status);
      console.log(xhr.responseText);
      var ourData =JSON.parse(xhr.responseText);
      renderHTML(ourData);
   }};

   function renderHTML(data){

    //Only "Vielen Dank, dass Du dich für die RegioApp entschieden hast!
    //Ab sofort kannst Du jederzeit, überall, ganz bequem regionale 
    //Lebensmittel und regionale Gastronomie finden." Als Ausgabe, wie in der App

 
   display.innerHTML = data.news;

   // Fussleiste unten fixiert
   const sheet = new CSSStyleSheet();
   sheet.replaceSync('#end {height: 5em; width: 100%; box-sizing: border-box; background-color: #b2bf2c; position: absolute; bottom: 0; text-align: center; padding-top: 1.2em;}');

   // Apply the stylesheet to a document:
   document.adoptedStyleSheets = [sheet];

}


xhr.send();