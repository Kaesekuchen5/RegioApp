# Die Webanwendung zur RegioApp

## Stand 06.04.2021: 
Die Entwicklungen zur Webanwendung sind fast abgeschlossen. Eventuelle Bugs oder Probleme werden derzeit (auch auf Anfrage) behoben.

## Kontakt:

- Albert Berberich: Berberic@students.uni-marburg.de
- Luca Kaiser:      KaiserTo@students.uni-marburg.de
