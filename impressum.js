var impressumT1 = document.getElementById("impressumText1");
impressumT1.innerText = "Bundesverband der Regionalbewegung e.V.\n Hindenburgstraße 11 \n 91555 Feuchtwangen"+
"\nTelefon: 09852 1381 \n Fax: 09852 615291 \n Der Verein ist eingetragen beim Registergericht Ansbach, Registernummer VR 1080.\n";

var impressumT2 = document.getElementById("impressumText2");
impressumT2.innerText = "Unterstützen Sie durch Ihre Spende die Arbeit des Bundesverbandes der Regionalbewegung! ";

var impressumT3 = document.getElementById("impressumText3");
impressumT3.innerText = "BLZ       765 910 00 \n Konto-Nr. 251909 \nIBAN         DE20 7659 1000 0000 2519 09 \nBIC           GENODEF1DKV";

var impressumT4 = document.getElementById("impressumText4");
impressumT4.innerText = "BLZ         765 500 00 \n Konto-Nr. 8057549 \nIBAN         DE46 7655 0000 0008 0575 49 \nBIC           BYLADEM1ANS";

