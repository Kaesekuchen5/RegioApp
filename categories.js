var url = "https://www.regionalbewegung.de/regioapp/data/categories-v2.php";
var display = document.getElementById("categories");

var xhr = new XMLHttpRequest();
xhr.open("GET", url);

xhr.onreadystatechange = function start() {
   if (xhr.readyState === 4) {
      console.log(xhr.status);
      console.log(xhr.responseText);
      var ourData =JSON.parse(xhr.responseText);
      renderHTML(ourData);
   }};

   function renderHTML(data){
    for (var x = 0; x < data.length; x++){
        var fig = document.createElement("figure");
        var figCap = document.createElement("figcaption");

        figCap.innerText = data[x].text;
        var image = document.createElement("img");
        var link = document.createElement('a');
        var id = data[x].id
        if (getParameterByName("place") != undefined) {
            link.setAttribute("href","selectedCategory_list.html?id=" + id + "&?place=" + getParameterByName("place"));
        } else {
            link.setAttribute("href","selectedCategory_list.html?id=" + id);
        }
        image.id = "category_picture";
        // Dorläden (Server problem)
        if (x == 2) {
           image.src = "https://www.regioapp.org/app/regioapp/images/categories/icon-dorflaeden.svg";
        } else {
           image.src = "https://regionalbewegung.de/regioapp/data/images/categories/" + data[x].image;
        }
        var display = document.getElementById("categories");

        fig.appendChild(image);
        fig.appendChild(figCap);
        fig.appendChild(link);
        
        link.appendChild(image);
        link.appendChild(figCap);
        display.appendChild(fig);
      }
}

xhr.send();

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, '\\$&');
   var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
   results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, ' '));
}