function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
  results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
var id = getParameterByName('id');
var url = "https://regionalbewegung.de/regioapp/data/profile-v4.php?id=" + id;
var display = document.getElementById("one_shop");

var xhr = new XMLHttpRequest();
var server_data;
xhr.open("GET", url);

xhr.onreadystatechange = function () {
  if (xhr.readyState === 4) {
    console.log(xhr.status);
    var ourData = server_data = JSON.parse(xhr.responseText);
  renderHTML(ourData);
  }};

  function renderHTML(data){
    // Map
    var shop_marker, own_marker;
    var infowindow = new google.maps.InfoWindow();

    const lats = parseFloat(data.lat);
    const lns = parseFloat(data.lng);
    const myLatLng = { lat: lats, lng:lns };
    const map = new google.maps.Map(document.getElementById("map"), {
      zoom: 15,
      center: myLatLng,
    });
    shop_marker = new google.maps.Marker({
      position: myLatLng,
      map,
      title: data.name,
    });
    shop_marker.addListener("click", () => {
      infowindow.setContent(data.name);
      infowindow.open(map, shop_marker);
    });

    // Own postion:
    var latitude, longitude;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
         function (position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            const own_position = {lat: latitude, lng: longitude};

            own_marker = new google.maps.Marker({
              position: own_position,
              icon: "https://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-a.png&scale=1.1",
              map,
              title: "Mein Standort",
            });

            own_marker.addListener("click", () => {
              infowindow.setContent("Dein Standort");
              infowindow.open(map, own_marker);
            });

            // Make all marker visible in one map
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(shop_marker.position);
            bounds.extend(own_marker.position);
            map.setCenter(bounds.getCenter);
            map.fitBounds(bounds);
         });
    } else {
      geoinfo.innerHTML = 'Dieser Browser unterstützt die Abfrage der Geolocation nicht.';
    }

    // Set new Title
    document.title = data.name;

    // Adress block
    var nameShop = document.getElementById("nameShop");
    nameShop.innerHTML = data.name;

    var infoShop = document.getElementById("one_shop");
    infoShop.innerHTML = data.betriebsart;

    var street = document.getElementById("street");
    street.innerHTML = data.strasse;

    var adress = document.getElementById("adress");
    adress.innerHTML = data.plz + " " + data.ort;

    // Contact block
    if (data.telefon1 !== (null || "")) {
      var tele = document.getElementById("tele1");
      tele.innerHTML = tele.href = data.telefon1;
    } else { // Remove
      var tele = document.getElementById("imageTele");
      var img = document.getElementById("imageTele");
      tele.remove();
      img.remove();
    }

    if (data.telefon2 !== (null || "")) {
      var tele = document.getElementById("tele2");
      tele.innerHTML = tele.href = data.telefon2;
    } else {  // Remove
      var tele = document.getElementById("tele2");
      tele.remove();
    }

    if (data.fax !== (null || "")) {
      var fax = document.getElementById("fax");
      fax.innerHTML = data.fax;
    } else {  // Remove
      var fax = document.getElementById("contact_fax");
      var img = document.getElementById("imagefax");
      fax.remove();
      img.remove();
    }

    if (data.mail !== (null || "")) {
      var mail = document.getElementById("mail");
      mail.innerHTML = mail.href = "mailto:" + data.mail;
    } else {  // Remove
      var mail = document.getElementById("contact_mail");
      var img = document.getElementById("imageMail");
      mail.remove();
      img.remove();
    }

    if (data.web !== (null || "")) {
      var homePage = document.getElementById("homepage");
      homePage.innerHTML = homePage.href = data.web;
    } else {  // Remove
      var homePage = document.getElementById("contact_homepage");
      var img = document.getElementById("imageHomepage");
      homePage.remove();
      img.remove();
    }

    if (data.facebook_url !== (null || "")) {
      var facebook = document.getElementById("facebook");
      facebook.innerHTML = facebook.href = data.facebook_url;
    } else {  // Remove
      var facebook = document.getElementById("contact_facebook");
      var img = document.getElementById("imageFacebook");
      facebook.remove();
      img.remove();
    }

    // Picture block
    var image_div = document.getElementById("profil_img_div");
    var lightbox = document.getElementById("lightbox");
    var lightbox_content = document.getElementById("lightbox_content");

    if (data.icon !== (null || "")) {
      var shopImage = document.createElement("img");
      shopImage.className = "profil_logo";
      shopImage.src = "https://regionalbewegung.de/regioapp/data/images/profiles/" + data.id + "/" + data.icon;
      shopImage.addEventListener("click", function() {openModal(); currentSlide(1);});
      image_div.appendChild(shopImage);

      var shopImagediv = document.createElement("div");
      shopImagediv.className = "shopImagediv";
      var shopImage2 = document.createElement("img");
      shopImage2.className = "profil_logo_lightbox";
      shopImage2.src = "https://regionalbewegung.de/regioapp/data/images/profiles/" + data.id + "/" + data.icon;
      shopImagediv.appendChild(shopImage2);
      lightbox_content.appendChild(shopImagediv)
      lightbox.appendChild(lightbox_content);
    }

    if (data.images.length !== 0) {
      for (var i = 0; i < data.images.length; i++) {
        var shopImage = document.createElement("img");
        shopImage.className = "profil_pictures";
        shopImage.src = "https://regionalbewegung.de/regioapp/data/images/profiles/" + data.id + "/" + data.images[i];
        if (data.icon !== (null || "")) {
          shopImage.addEventListener("click", listener.bind(null, i + 2));
        } else {
          shopImage.addEventListener("click", listener.bind(null, i + 1));
        }
        image_div.appendChild(shopImage);

        var shopImagediv = document.createElement("div");
        shopImagediv.className = "shopImagediv";
        var shopImage2 = document.createElement("img");
        shopImage2.className = "profil_pictures_lightbox";
        shopImage2.src = "https://regionalbewegung.de/regioapp/data/images/profiles/" + data.id + "/" + data.images[i];
        shopImagediv.appendChild(shopImage2);
        lightbox_content.appendChild(shopImagediv);
        lightbox.appendChild(lightbox_content);
      }
    }

    // Picture block is empty
    if ((data.icon === (null || "")) && (data.images.length === 0)) {
      image_div.remove();
    }

    // About us
    var inhaber = document.getElementById("inhaber");
    inhaber.innerHTML = "Inhaber: " + data.inhaber;

    var aboutUs = document.getElementById("aboutShopText");
    aboutUs.innerHTML = data.text;

    // Open time
    var openTimeText = document.getElementById("openTimeText");
    openTimeText.innerHTML = data.oeffnungszeiten;

    var openTime1 = document.getElementById("Mo1");
    var openTime2 = document.getElementById("Mo2");
    if (data.open["Mo1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["Mo1"];
      if (data.open["Mo2"] != undefined) {
        openTime2.innerHTML = data.open["Mo2"];
      } else {
        openTime2.remove();
      }
    }

    var openTime1 = document.getElementById("Di1");
    var openTime2 = document.getElementById("Di2");
    if (data.open["Di1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["Di1"];
      if (data.open["Di2"] != undefined) {
        openTime2.innerHTML = data.open["Di2"];
      } else {
        openTime2.remove();
      }
    }

    var openTime1 = document.getElementById("Mi1");
    var openTime2 = document.getElementById("Mi2");
    if (data.open["Mi1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["Mi1"];
      if (data.open["Mi2"] != undefined) {
        openTime2.innerHTML = data.open["Mi2"];
      } else {
        openTime2.remove();
      }
    }

    var openTime1 = document.getElementById("Do1");
    var openTime2 = document.getElementById("Do2");
    if (data.open["Do1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["Do1"];
      if (data.open["Do2"] != undefined) {
        openTime2.innerHTML = data.open["Do2"];
      } else {
        openTime2.remove();
      }
    }

    var openTime1 = document.getElementById("Fr1");
    var openTime2 = document.getElementById("Fr2");
    if (data.open["Fr1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["Fr1"];
      if (data.open["Fr2"] != undefined) {
        openTime2.innerHTML = data.open["Fr2"];
      } else {
        openTime2.remove();
      }
    }

    var openTime1 = document.getElementById("Sa1");
    var openTime2 = document.getElementById("Sa2");
    if (data.open["Sa1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["Sa1"];
      if (data.open["Sa2"] != undefined) {
        openTime2.innerHTML = data.open["Sa2"];
      } else {
        openTime2.remove();
      }
    }

    var openTime1 = document.getElementById("So1");
    var openTime2 = document.getElementById("So2");
    if (data.open["So1"] == undefined){
      openTime1.innerHTML = "geschlossen";
      openTime2.remove();
    } else {
      openTime1.innerHTML = data.open["So1"];
      if (data.open["So2"] != undefined) {
        openTime2.innerHTML = data.open["So2"];
      } else {
        openTime2.remove();
      }
    }

    // Specials of shop
    if (data.produkte !== "") {
      var spezi = document.getElementById("spezi");
      spezi.innerHTML = data.produkte;
    } else {  //Remove
      var spezi_div = document.getElementById("spezi_div_remove");
      var segregation = document.getElementById("segregation_remove_1");
      spezi_div.remove();
      segregation.remove();
    }

    // Product info
    if (data.product_info.length !== 0) {
      var producer_name = document.getElementById("producer_name");
      producer_name.innerHTML = data.name;

      var productlist = document.getElementById("productInfo");
      for (var x = 0; x < data.product_info.length; x++) {

        // Product
        var products = document.createElement("h4");
        products.className = "products";
        products.innerHTML = data.product_info[x].name;

        productlist.appendChild(products);

        // Shops with product
        for (var i = 0; i < data.product_info[x].erzeuger.length; i++) {
          var shops = document.createElement("a");
          shops.className = "productInfo_shops";
          shops.innerHTML = "• " + data.product_info[x].erzeuger[i].name;

          if (data.product_info[x].erzeuger[i].name !== "Eigenproduktion") {
            shops.href = "selectedShop.html?id=" + data.product_info[x].erzeuger[i].target;
          }

          productlist.appendChild(shops);
        }
      }
    } else {  //Remove
      var product_info_div = document.getElementById("productInfo_div_remove");
      var segregation = document.getElementById("segregation_remove_2");
      product_info_div.remove();
      segregation.remove();
    }

    // Certification
    if (data.certifications.length !== 0) {
      var certi = document.getElementById("certi");
      for (var i = 0; i < data.certifications.length; i++) {
        var certification = document.createElement("img");
        certification.className = "certi_img";
        certification.src = "https://regionalbewegung.de/regioapp/data/images/certifications/" + data.certifications[i].icon;
        
        certi.appendChild(certification);
      }
    } else {  //Remove
      var certification_div = document.getElementById("certi_div_remove");
      var segregation = document.getElementById("segregation_remove_3");
      certification_div.remove();
      segregation.remove();
    }

    // Verbandsmitgliedschaften
    var shop_name = document.getElementById("verbandInfo_name");
    shop_name.innerHTML = data.name;

    var verbandimg_div = document.getElementById("verbandInfo");
    for (var i = 0; i < data.verband.length; i++) {
      var fig = document.createElement("figure");
      fig.className = "fig_verbandInfo";
      var verbandimg = document.createElement("img");
      
      var verband_link = document.createElement("a");
      verband_link.href = data.verband[i].url;
      
      verbandimg.className = "verbandInfo_img";
      verbandimg.src = "https://regionalbewegung.de/regioapp/data/images/verband/" + data.verband[i].icon;

      fig.appendChild(verbandimg);
      fig.appendChild(verband_link);

      verband_link.appendChild(verbandimg);
      verbandimg_div.appendChild(fig);
    }

    // Responsible
    var responsible_name_1 = document.getElementById("responsible_name_1");
    responsible_name_1.innerHTML = data.name;

    var responsible_name_2 = document.getElementById("responsible_name_2");
    responsible_name_2.innerHTML = data.responsible;
  }

xhr.send();

// Index-listener for addEventListener()
function listener(index) {
  openModal(); 
  currentSlide(index);
}

// Button to Google Maps
function navigationButton() {
  window.location = "https://maps.google.de/maps?q=" + server_data.strasse + " " + server_data.plz + " " + server_data.ort;
}

// Open lightbox
function openModal() {
  document.getElementById("lightbox").style.display = "block";
}

// Close lightbox
function closeModal() {
  document.getElementById("lightbox").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  console.log(n);
  var slides = document.getElementsByClassName("shopImagediv");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (var i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
    if (i == (slideIndex-1)) {
      slides[slideIndex-1].style.display = "block";
    }
  }
}